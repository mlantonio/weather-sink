package com.flowtable.pojo;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Message implements Serializable {
    private String eventName;
    private LocalDateTime timestamp;
    private Map<String, Object> data;
}
